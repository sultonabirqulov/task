from django.db import models

# Create your models here.


class Faculty(models.Model):
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    image = models.ImageField(upload_to='faculties/images', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz