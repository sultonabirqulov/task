from django.contrib import admin
from .models import Faculty
# Register your models here.


class FacultyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name_uz', 'name_ru')
    list_display_links = ('id', 'name_uz')


admin.site.register(Faculty, FacultyAdmin)
