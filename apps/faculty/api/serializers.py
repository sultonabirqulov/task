from apps.faculty.models import Faculty
from rest_framework import serializers
from django.utils.translation import get_language
from apps.student.api.serialziers import GroupSerializer

class FacultiesListSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Faculty
        fields = ('id', 'name', 'image')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        if lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class FacultiesGroupSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    name = serializers.SerializerMethodField()

    class Meta:
        model = Faculty
        fields = ('id', 'name', 'image', 'groups')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        if lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz