from apps.faculty.models import Faculty
from apps.faculty.api.serializers import FacultiesListSerializer, FacultiesGroupSerializer
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status


class FacultiesListAPIView(ListAPIView):
    serializer_class = FacultiesListSerializer
    queryset = Faculty.objects.all()


class FacultyGroupAPIView(APIView):
    serializer_class = FacultiesGroupSerializer

    def get(self, request):
        faculty_id = self.request.query_params.get('faculty_id')
        try:
            faculty = Faculty.objects.get(id=int(faculty_id))
            seralizer = self.serializer_class(faculty, context={'request': request})
            res = {
                'status': 1,
                'results': seralizer.data
            }
            return Response(res, status=status.HTTP_200_OK)
        except Faculty.DoesNotExist:
            return Response({'Faculty does not exist'}, status=status.HTTP_400_BAD_REQUEST)
