from django.urls import path
from apps.faculty.api.views import FacultiesListAPIView, FacultyGroupAPIView


urlpatterns = [
    path('faculties', FacultiesListAPIView.as_view()),
    path('faculty_group', FacultyGroupAPIView.as_view()),
]