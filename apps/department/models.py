from django.db import models
from apps.faculty.models import Faculty
# Create your models here.


class Department(models.Model):
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, null=True, blank=True, related_name='departments')
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz