from django.contrib import admin
from .models import Department
# Register your models here.


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name_uz', 'name_ru')
    list_display_links = ('id', 'name_uz')


admin.site.register(Department, DepartmentAdmin)
