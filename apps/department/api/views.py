from apps.department.models import Department
from rest_framework.generics import ListAPIView
from apps.department.api.serializers import DepartmentSerializer


class DepartmentAPIView(ListAPIView):
    serializer_class = DepartmentSerializer
    queryset = Department.objects.all()