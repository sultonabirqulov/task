from apps.department.models import Department
from rest_framework import serializers
from django.utils.translation import get_language


class DepartmentSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    faculty_name = serializers.StringRelatedField(source='filial_name_uz')

    class Meta:
        model = Department
        fields = ('id', 'faculty', 'faculty_name', 'name')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz