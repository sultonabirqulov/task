from django.urls import path
from apps.department.api.views import DepartmentAPIView


urlpatterns = [
    path('departments', DepartmentAPIView.as_view())
]