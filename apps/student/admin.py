from django.contrib import admin
from apps.student.models import Student, Region, City, Direction, Group
# Register your models here.


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'full_name', 'passport', 'birthday')
    list_display_links = ('id', 'full_name', 'passport', 'birthday')


admin.site.register(Student, StudentAdmin)
admin.site.register(Region)
admin.site.register(City)
admin.site.register(Direction)
admin.site.register(Group)
