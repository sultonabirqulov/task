from django.urls import path
from apps.student.api.views import login, token_refresh, RegionAPIView, CityAPIView, GroupAPIView, DirectionAPIView, \
    StudentCreateAPIView, GroupStudentAPIView, StudentAPIView, StudentDeleteAPIView, GeneratePDFView


urlpatterns = [
    path('login', login),
    path('token_refresh', token_refresh),
    path('regions', RegionAPIView.as_view()),
    path('cities', CityAPIView.as_view()),
    #group
    path('groups', GroupAPIView.as_view()),
    path('group_students', GroupStudentAPIView.as_view()),

    path('directions', DirectionAPIView.as_view()),
    #student
    path('student_create', StudentCreateAPIView.as_view()),
    path('student_detail', StudentAPIView.as_view()),
    path('student_delete', StudentDeleteAPIView.as_view()),
    path('generate_pdf', GeneratePDFView.as_view()),

]