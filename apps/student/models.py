from django.db import models
from apps.faculty.models import Faculty
from apps.department.models import Department
from config.contract import COURSE_BACHELOR, COURSE_POSTGRADUATE
# Create your models here.


class Region(models.Model):
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz


class City(models.Model):
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz


class Direction(models.Model):
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz


class Group(models.Model):
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, null=True, blank=True, related_name='groups')
    name_uz = models.CharField(max_length=128, null=True, blank=True)
    name_ru = models.CharField(max_length=128, null=True, blank=True)
    name_en = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name_uz


class Student(models.Model):
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE, null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, blank=True, related_name='students')
    direction = models.ForeignKey(Direction, on_delete=models.CASCADE, null=True, blank=True)
    course = models.CharField(max_length=128, null=True, blank=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=True, blank=True)
    area = models.CharField(max_length=256, null=True, blank=True)
    full_name = models.CharField(max_length=128, null=True, blank=True)
    first_name = models.CharField(max_length=128, null=True, blank=True)
    last_name = models.CharField(max_length=12, null=True, blank=True)
    email = models.EmailField(max_length=128, null=True, blank=True)
    phone = models.IntegerField(null=True, blank=True, unique=True)
    birthday = models.CharField(max_length=128, null=True, blank=True)
    photo = models.ImageField(upload_to='student/images', null=True, blank=True)
    passport = models.CharField(max_length=128, null=True, blank=True)
    pinfl = models.IntegerField(null=True, blank=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name


