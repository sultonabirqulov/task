from apps.student.models import Student, Region, City, Direction, Group
from apps.student.api.serialziers import RegionSerializer, CitySerializer, DirectionSerializer, GroupSerializer, \
    StudentSerializer, StudentListSerializer, GroupStudentSerializer
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken, AccessToken
from rest_framework.generics import ListAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from reportlab.pdfgen import canvas
from django.http import HttpResponse


@api_view(['POST'])
@permission_classes([AllowAny, ])
def login(request):
    passport = request.data['passport']
    birthday = request.data['birthday']
    try:
        student_filter = Student.objects.filter(passport=passport, birthday=birthday)
        if student_filter:
            student = Student.objects.filter(passport=passport, birthday=birthday).first()
            refresh = RefreshToken.for_user(student)
            res = {
                'status': 1,
                'msg': "User sing in",
                'access': str(refresh.access_token),
                'refresh': str(refresh),
                'token_type': "bearer",
                'access_date': 300,
                'refresh_date': 86400
            }
            return Response(res, status=status.HTTP_200_OK)
        else:
            res = {
                'status': 0,
                'msg': "Student does not found"
            }
            return Response(res, status=status.HTTP_200_OK)
    except KeyError:
        res = {
            'status': 0,
            'msg': 'Data error'
        }
        return Response(res, status=status.HTTP_403_FORBIDDEN)


@api_view(['POST'])
@permission_classes([AllowAny, ])
def token_refresh(request):
    try:
        token = request.data['token']
        token_type = request.data['token_type']
        if token_type == 'access':
            access_token_obj = AccessToken(token)
            student_id = access_token_obj['user_id']
            student = Student.objects.get(id=student_id)
        elif token_type == 'refresh':
            refresh_token_obj = RefreshToken(token)
            student_id = refresh_token_obj['user_id']
            student = Student.objects.get(id=student_id)
        else:
            res = {
                'status': 0,
                'msg': "Bu token topilmadi"
            }
            return Response(res)
        if student:
            def get_token():
                return RefreshToken.for_user(student)

            refresh = get_token()
            res = {
                'user_id': student.id,
                'status': 1,
                'msg': 'Refresh token',
                'access': str(refresh.access_token),
                'refresh': str(refresh),
                'token_type': "bearer",
                'access_date': 300,
                'refresh_date': 86400
            }
            return Response(res)
        else:
            res = {
                'status': 0,
                'msg': 'User none'
            }
            return Response(res, status=status.HTTP_403_FORBIDDEN)
    except KeyError:
        res = {
            'status': 0,
            'error': 'Please provide a login and a password'
        }
        return Response(res, status=status.HTTP_403_FORBIDDEN)


class RegionAPIView(ListAPIView):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()


class CityAPIView(ListAPIView):
    serializer_class = CitySerializer
    queryset = City.objects.all()


class GroupAPIView(ListAPIView):
    serializer_class = GroupSerializer
    queryset = Group.objects.all()


class DirectionAPIView(ListAPIView):
    serializer_class = DirectionSerializer
    queryset = Direction.objects.all()


class StudentCreateAPIView(APIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = StudentSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            res = {
                'status': 1,
                'msg': 'Student create successfully',
                'results': serializer.data
            }
            return Response(res, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StudentAPIView(APIView):
    permission_classes = [IsAuthenticated, ]
    serializer_class = StudentListSerializer

    def get(self, request):
        student_id = self.request.query_params.get('student_id')
        try:
            student = Student.objects.get(id=int(student_id))
            serializer = self.serializer_class(student, context={'request': request})
            res = {
                'status': 1,
                'results': serializer.data
            }
            return Response(res, status=status.HTTP_201_CREATED)
        except Student.DoesNotExist:
            return Response({'Student does not exist'}, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request):
        student_id = self.request.query_params.get('student_id')
        try:
            student = Student.objects.get(id=int(student_id))
            serializer = StudentSerializer(student, data=request.data,  context={'request': request})
            if serializer.is_valid():
                serializer.save()
                serializer = StudentListSerializer(student)
                res = {
                    'status': 1,
                    'message': 'Student update successfully',
                    'results': serializer.data
                }
                return Response(res, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Student.DoesNotExist:
            return Response({'Student does not exist'}, status=status.HTTP_400_BAD_REQUEST)


class StudentDeleteAPIView(APIView):
    permission_classes = [IsAuthenticated, ]

    def delete(self, request):
        student_id = self.request.query_params.get('student_id')
        try:
            student = Student.objects.get(id=int(student_id))
            student.delete()
            res = {
                'status': 1,
                'msg': 'Student delete successfully'
            }
            return Response(res, status=status.HTTP_201_CREATED)
        except Student.DoesNotExist:
            return Response({'Student does not exist'}, status=status.HTTP_400_BAD_REQUEST)


class GroupStudentAPIView(APIView):
    serializer_class = GroupStudentSerializer

    def get(self, request):
        group_id = self.request.query_params.get('group_id')
        try:
            faculty = Group.objects.get(id=int(group_id))
            seralizer = self.serializer_class(faculty, context={'request': request})
            res = {
                'status': 1,
                'results': seralizer.data
            }
            return Response(res, status=status.HTTP_200_OK)
        except Group.DoesNotExist:
            return Response({'Faculty does not exist'}, status=status.HTTP_400_BAD_REQUEST)


class GeneratePDFView(APIView):
    def generate_student_pdf(self, student):
        response = HttpResponse(content_type='application/pdf')
        pdf_filename = f"{student.first_name}_{student.last_name}.pdf"
        response['Content-Disposition'] = f'attachment; filename="{pdf_filename}"'

        p = canvas.Canvas(response)

        student_info = f"Full Name: {student.full_name}\nEmail: {student.email}\nPhone: {student.phone}"
        p.drawString(100, 700, student_info)

        p.showPage()
        p.save()

        return response

    def get(self, request):
        student_id = self.request.query_params.get('student_id')
        try:
            student = Student.objects.get(id=student_id)
        except Student.DoesNotExist:
            return Response({'Student does not exist'}, status=status.HTTP_400_BAD_REQUEST)

        response = self.generate_student_pdf(student)
        return response