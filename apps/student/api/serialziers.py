from apps.student.models import Student, Region, City, Direction, Group
from rest_framework import serializers
from django.utils.translation import get_language


class RegionSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Region
        fields = ('id', 'name')

    def get_name(self, obj):
        lan = get_language()
        print(lan)
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class CitySerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = City
        fields = ('id', 'name')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class DirectionSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    class Meta:
        model = Direction
        fields = ('id', 'name')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class GroupSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    faculty_name = serializers.StringRelatedField(source='faculty_name_uz')

    class Meta:
        model = Group
        fields = ('id', 'faculty', 'faculty_name', 'name')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class StudentGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'full_name', 'first_name', 'last_name', 'email', 'phone', 'birthday')


class GroupStudentSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    students = StudentGroupSerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'students')

    def get_name(self, obj):
        lan = get_language()
        if lan == 'ru':
            return obj.name_ru
        elif lan == 'en':
            return obj.name_en
        else:
            return obj.name_uz


class StudentListSerializer(serializers.ModelSerializer):
    faculty = serializers.StringRelatedField(source='faculty.name_uz')
    group = serializers.StringRelatedField(source='group.name_uz')
    direction = serializers.StringRelatedField(source='direction.name_uz')
    region = serializers.StringRelatedField(source='region.name_uz')
    city = serializers.StringRelatedField(source='city.name_uz')

    class Meta:
        model = Student
        fields = ('id', 'faculty', 'group', 'direction', 'course', 'region', 'city', 'area', 'full_name', 'first_name', 
                  'last_name', 'email', 'phone', 'birthday', 'photo', 'passport', 'pinfl')


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'faculty', 'group', 'direction', 'course', 'region', 'city', 'area', 'full_name', 'first_name',
                  'last_name', 'email', 'phone', 'birthday', 'photo', 'passport', 'pinfl')